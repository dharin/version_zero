class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  #validation    
  validates :email, uniqueness: { case_sensitive: false }
  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(:email => data["email"]).first
    unless user
        user = User.create(email: data["email"],
           password: Devise.friendly_token[0,20],
           first_name: data['first_name'],
           last_name: data['last_name']
        )
    end
    user
  end
end
